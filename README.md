# macrominds/provision/uberspace/web-link-html-to-deployer-target

Prepare an U7 Uberspace server docroot to work with our deployer autodeployment: 

* turn the html folder into a link to the deployer target's public directory (`current/public`)

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `web_link_html_path`: the path to the symlink (on a fresh Uberspace, this is an existing folder)
  Defaults to `{{ web_link_base_path }}/html`
* `web_link_base_path`: the parent path to the symlink
  Defaults to `/var/www/virtual/{{ ansible_facts.user_id }}`.
* `web_link_deployer_target_docroot`: the actual target of the symlink. 
  Can be absolute or relative to the `web_link_base_path`.
  Defaults to `{{ web_link_deployer_target_base_path }}/public`.
* `web_link_deployer_target_base_path`: the base path of the target of the symlink. 
  Can be absolute or relative to the `web_link_base_path`.
  Defaults to `current`.


## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/web-link-html-to-deployer-target.git
  path: roles
  name: web-link-html-to-deployer-target
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: web-link-html-to-deployer-target
```

## Testing

Test this role with `molecule test --all`.

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
